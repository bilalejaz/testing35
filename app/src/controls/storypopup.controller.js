'use strict';

module.exports = controller;

/* @ngInject */
function controller($scope) {
    var vm = this;
    vm.IsManager = parent.IsManager;
    vm.IsEditorial = parent.IsEditorial;
    vm.slot = parent.popupSlot;
    vm.newsId = parent.popupNewsId;

    if (!vm.slot.Guest || vm.slot.Guest.length == 0) {
        vm.slot.Guest = [];
        vm.slot.Guest.push({});
    }
    vm.slot.EditorialComments = parent.SetEditorialComments(vm.slot.EditorialComments);

    //if (vm.slot.EditorialComments && vm.slot.EditorialComments.length > 0) {
    //    vm.slot.EditorialComments = _.reverse(_.sortBy(vm.slot.EditorialComments, 'EditorialCommentId'));
    //    $.each(vm.slot.EditorialComments, function (i, v) {
    //        v.Created = moment(v.CreationDateStr).fromNow();
    //        v.UserName = userservice.getUserName(v.EditorId);
    //    });
    //}

    vm.IsActiveGuests = function () {
        return _.filter(vm.slot.Guest, function (g) { return g.CelebrityId > 0; }).length > 0;
    }

    vm.addGuest = function () {
        var a = vm.slot.Guest.length;
        vm.slot.Guest.splice(0, 0, {});
    }

    vm.closeDialog = function () {
        //  $mdDialog.hide();
    }


    vm.save = function () {
        var slot = vm.slot;
        var tempObj = {};
        var slotsToPush = [];
        tempObj["SlotId"] = slot.SlotId;
        tempObj["SequnceNumber"] = slot.SequenceNumber || 0;
        tempObj["SegmentId"] = slot.SegmentId;
        tempObj["NewsFileId"] = slot.NewsFileId;
        tempObj["IncludeInRundown"] = 0;
        tempObj["CategoryId"] = slot.CategoryId;
        tempObj["EpisodeId"] = parent.episodeId;
        tempObj["Title"] = slot.Title;
        tempObj["Description"] = slot.Description;
        // tempObj["Guests"] = [{ CelebrityId: 1, Question: 'abcd', LocationId: 1 }]

        slotsToPush.push(tempObj);
        parent.userData = localStorageService.get('userInfo');


        var requestObj = {
            Slots: slotsToPush,
            UserId: parent.userData.UserId,
            UserRole: parent.userData.FullName,
        };

        dataservice.updateStory(requestObj).then(function (data) {
            if (data.IsSuccess) {
                //parent.onEpisodeDateChange(parent.currentDate);
                //parent.onEpisodeChange(parent.episodeId);
            }
        });
        vm.InsertGuest();
    };

    vm.onSelectCelebrity = function (celebrityId) {
        parent.celebrityId = celebrityId;
    };
    vm.InsertGuest = function () {
        var slot = vm.slot;
        var reqObj = {
            SlotId: slot.SlotId,
            Guests: _.filter(vm.slot.Guest, function (g) { return g.CelebrityId > 0; })
        }


        $.each(reqObj.Guests, function (i, v) { v.SlotId = slot.SlotId, v.GuestId = undefined });

        dataservice.InsertGuest(reqObj).then(function (data) {
            //if (data.IsSuccess)
            // parent.newsList = data.Data.NewsFile;
        });
    };
    vm.InsertComment = function () {
        if (vm.EditorialComment && vm.EditorialComment.length > 0) {
            $('.sendText').css('border', '#e3e3e4 1px solid');
            var obj = {
                Comment: vm.EditorialComment,
                EditorId: userservice.getUserId(),
                NewsFileId: vm.slot.NewsFileId,
                SlotId: vm.slot.SlotId
            }
            dataservice.AddSlotComment(obj).then(function (data) {
                if (data.IsSuccess) {
                    vm.EditorialComment = '';

                    if (!vm.slot.EditorialComments) {
                        vm.slot.EditorialComments = [];
                    }
                    vm.slot.EditorialComments.splice(0, 0, data.Data.EditorialComments[0]);
                    vm.slot.EditorialComments = parent.SetEditorialComments(vm.slot.EditorialComments);
                }
            })
        } else {
            $('.sendText').css('border', '#FF0000 1px solid');
        }
    }
    vm.GetGuestDetails = function () {


    };
    vm.closePopup = function () {
        parent.showdetailspopup = false;
    }
}
