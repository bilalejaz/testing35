﻿'use strict';

module.exports = directive;

/* @ngInject */
function directive($rootScope, $log, dataservice, $state) {

    return {
        link: link,
        restrict: 'E',
        template: require('./guestlist.directive.html'),
        scope: {
            guest: '='
        }
    };

    function link(scope, elem, attrs) {
        var a = scope.guest;
        //scope.guest.guestStartTime = moment(scope.guest.guestStartTime).format("hh:mm a");
        //scope.guest.guestEndTime = moment(scope.guest.guestEndTime).format("hh:mm a");

        if (scope.guest.medium == "1")
            scope.guest.mediumText = 'Studio';
        else if (scope.guest.medium == "2")
            scope.guest.mediumText = 'DSNG';
        else if (scope.guest.medium == "3")
            scope.guest.mediumText = 'Beeper';
        else
            scope.guest.mediumText = 'No Medium';


        if (scope.guest.GuestImage) {
            scope.guest.GuestImage = scope.guest.GuestImage ? 'http://10.3.12.119/api/resource/getthumb/' + scope.guest.GuestImage : 'http://10.3.12.118/content/images/producer/noimage-730x380.jpg';
        }
    }

}