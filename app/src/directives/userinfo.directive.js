'use strict';

module.exports = directive;

/* @ngInject */
function directive($log,localStorageService) {
     //console.log(localStorageService.get('userName'));
  return {
    link: link,
    restrict: 'AE',
    template: require('./userinfo.directive.html'),
    scope: {
      test: '='
    }
  };

  function link(scope, elem, attrs) {
      
    scope.userData = localStorageService.get('userInfo');
    if (scope.userData && scope.userData.FullName) {
        scope.username = scope.userData.FullName;
    }
    if (scope.userData && scope.userData.Modules[0] && scope.userData.Modules[0].WorkRoleName) {
        scope.designation = scope.userData.Modules[0].WorkRoleName;
    }
    scope.imageSource = './assets/images/avatar-user.png';

  }
}
