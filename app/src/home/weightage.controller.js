﻿'use strict';

module.exports = controller;
/* @ngInject */
function controller($log, Restangular, dataservice, toastr, $state, $location, $timeout, rabbitmq, $scope, Config, $rootScope) {

    var vm = this;
    vm.currDate = '';
    vm.BroadcastedProgramData = [];
    vm.Loader = true;
    vm.IsIconChecked = false;
    vm.AllSelfieIds = [];
    vm.OgList = [];
    vm.currentIndex = 0;
    vm.AllSelfies = [];
    vm.stDate = '';
    vm.enDate = '';

    function Init() {
       
    }
    $scope.go = function ()
    {
        if (!$scope.isgroup1)
            $scope.isgroup2 = null;

        var weightage = {
            KarachiWeightage : $scope.karachiweightage,
            LahoreWeightage : $scope.lahoreweightage,
            IslamabadWeightage : $scope.islamabadweightage,
            OthersWeightage : $scope.otherweightage,
            isgroup1: $scope.isgroup1,
            isgroup2: $scope.isgroup2,
            UserResponse : $scope.userresponse,
            SelfieCount: $scope.selfiecount,
            ExecutionDate: vm.stDate
        };
        dataservice.Sendweightage(weightage).then(function (PriceType) {
            alert("success");
        });
        
    }
    vm.onDateRangeChange = function (startDate, endDate) {
        vm.Date = '';
        var dd = startDate.getDate();
        var mm = startDate.getMonth() + 1;
        var yyyy = startDate.getFullYear();
        var stdate = yyyy + '/' + mm + '/' + dd;
        vm.stDate = stdate.toString();

        var edd = endDate.getDate();
        var emm = endDate.getMonth() + 1;
        var eyyyy = endDate.getFullYear();
        var endate = eyyyy + '/' + emm + '/' + edd;
        vm.enDate = endate.toString();

       // vm.LoadMoreData(vm.stDate, vm.enDate);
    }

  

    Init();
}