'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, userservice) {

    return {
        link: link,
        restrict: 'E',
        template: require('./newsListItem.directive.html'),
        scope: {
            newsItem: '=',
            type: '@',
        }
    };

    function link(scope, elem, attrs) {
        if (scope.type == 'program-news') {
            scope.newsItem.styleClass = 'programnews';
        }
        if (scope.newsItem.CreatedBy) {
            scope.newsItem.RerportedBy = userservice.getUserName(scope.newsItem.CreatedBy || 0);
        }
    }
}
