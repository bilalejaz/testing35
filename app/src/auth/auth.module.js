'use strict';

//require('./auth.scss');

var name = module.exports = 'BolSelfie.auth';

angular
  .module(name, [])
  .config(configuration)
  .controller('Auth', require('./auth.controller.js'))
  .controller('Login', require('./login.controller.js'))
;

function configuration($stateProvider) {
    $stateProvider
      .state('auth', {
          url: '/auth',
          template: require('./auth.html'),
          controller: 'Auth as vm',
          title: 'Welcome to Wayne Minor.'
      }).state('login', {
          url: '/login',
          template: require('./login.html'),
          controller: 'Login as vm',
          title: 'Login'
      });
}
