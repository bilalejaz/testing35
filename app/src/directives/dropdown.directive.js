'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice, $rootScope) {
    return {
        link: link,
        restrict: 'E',
        template: require('./dropdown.directive.html'),
        scope: {
            onSelect: '&',
            options: '=',
            autoSelect: '@',
            model: '=',
            placeholder: '@'
        }
    };


    function link(scope, elem, attrs) {

        var data = [];

        if (scope.options && scope.options.length > 0) {
            $.each(options, function (i, v) {
                data.push({ id: v.id, text: v.text, name: v.text, selected: i == 0 && scope.autoSelect ? true : false });
            });
        }

        function rebindSelect() {
            var cat = $(elem).find("select");



            if (data.length > 0) {
                scope.onSelect({ 'id': data[0].id });
                $rootScope.safeApply(function () {
                    scope.model = data[0].id;
                });
            }
            cat.select2({
                data: data,
                minimumResultsForSearch: Infinity
            });
            cat.on('select2:select', function (evt) {
                scope.onSelect({ 'id': cat.val() });
                $rootScope.safeApply(function () {
                    scope.model = cat.val();
                });
            });
        }

        setTimeout(function () {
            rebindSelect();
        }, 300);

        scope.$watch(function (scope) { return scope.options; }
            , function (newVal, oldVal) {
                if (newVal != oldVal) {
                    
                    $(elem).find("select option").remove();
                    data = scope.options;
                    rebindSelect();
                }
            });
    }

}
