'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice, Restangular, userservice) {
    return {
        link: link,
        restrict: 'E',
        template: require('./panelAddUpdateSegment.directive.html'),
        scope: {
            currentGuestData: '=',
            filledDetailType: '=',
            markGuest: '&',
            mediumOptions: '=',
            clearData: '='
        }
    };


    function link(scope, elem, attrs, $rootScope) {
        scope.selectionUpdate = function (item) {
            debugger;
        }
    }
}
