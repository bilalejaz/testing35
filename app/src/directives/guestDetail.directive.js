'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice, Restangular, userservice) {
    return {
        link: link,
        restrict: 'E',
        template: require('./guestDetail.directive.html'),
        scope: {
            currentGuestData: '=',
            filledDetailType: '=',
            markGuest: '&',
            mediumOptions: '=',
            clearData: '='
        }
    };


    function link(scope, elem, attrs, $rootScope) {

        scope.FinalMedium = 0;
        if (scope.currentGuestData) {
            scope.currentGuestData.programDatetime = moment(scope.currentGuestData.programDatetime).format('LLL');
            scope.emptyDv = false;
            scope.filledDv = true;
        }

        if (!scope.currentGuestData) {
            scope.emptyDv = true;
            scope.filledDv = false;
        }

        scope.altOpen = function (data) {
            scope.rejectComment = '';
            $('body').find('.boxActive').removeClass('boxActive');
            $('body').find('.checkActi').addClass('boxActive');

            $('body').addClass('showSidebar');
            $('.overlay').show();
            scope.CurrentGuest = data;
        }

        scope.altClose = function (data) {
            scope.rejectComment = '';
            $('body').find('.boxActive').removeClass('boxActive');
            $('body').find('.closeActi').addClass('boxActive');

            $('body').addClass('showSidebar');
            $('.overlay').show();
            scope.CurrentGuest = data;
        }

        scope.ScheduledGuest = function (CurrentGuest, Status) {
            if (!scope.finalSelectedMedium)
                scope.finalSelectedMedium = CurrentGuest.curnMd;

            scope.markGuest({ 'guestdata': CurrentGuest, 'Status': Status, 'SelectedMedium': scope.finalSelectedMedium, 'rejectComment': scope.rejectComment });
        }

        scope.selectionUpdate = function (selectedoption) {
            scope.finalSelectedMedium = selectedoption;
            scope.CurrentGuest.curnMd = scope.finalSelectedMedium;
        }

        scope.$watch('clearData', function () {
            if (scope.clearData == true) {
                debugger;
                $('.detailsSec').addClass('dnone');
                $('.blankScreen').removeClass('dnone');
                //scope.currentGuestData = [];

                $('body').removeClass('showSidebar');
                $('.overlay').hide();
            }
        });


        $('.overlay').click(function () {
            //$('.detailsSec').addClass('dnone');
            //$('.blankScreen').removeClass('dnone');
            $('body').removeClass('showSidebar');
            $('.overlay').hide();
        });
    }
}
