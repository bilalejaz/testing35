'use strict';

module.exports = directive;

/* @ngInject */
function directive($log,Upload,$timeout,Restangular,localStorageService,$stateParams) {
  return {
    link: link,
    restrict: 'AE',
    template: require('./fileUpload.directive.html'),
    scope: {
        fileAdded: '&',
        clearText: '=',
        openViewer: '&'
    }
  };

  function link(scope, elem, attrs) {
      scope.totalFiles = [];
    var newsId = $stateParams.newsId;
      // eslint-disable-line no-undef
      //$log.info('Initializing directive');
      scope.totalFiles = [];
      scope.userData = localStorageService.get('userInfo');
      //console.log($stateParam);
      if (newsId)
        getNewsResources(newsId);
      
              
      scope.$watch('clearText', function () {
          if (scope.clearText == true) {
              scope.totalFiles = [];
          }
      });


      scope.showIframe = function () {
          $(".pass_popup1").css('display', 'initial');
      }

      scope.uploadFiles = function(files, errFiles) {
        scope.files = files;
        scope.errFiles = errFiles;
        angular.forEach(files, function(file) {
          //get file resourceIds;
          var guid = GetResourceIds(file.name).then(function(response){
            //console.log(response.Data[0].Guid);
              if (response.IsSuccess && response.Data && response.Data.length > 0) {
                  var currItem = response.Data[0];
                file.upload = Upload.upload({
                  //url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                    //url: 'http://localhost:1888/api/ResourceUploader/',
                    url: 'http://10.3.12.118/api/ResourceUploader/',
                  data: {file: file},
                  headers: {'ResourceGuid': response.Data[0].Guid}
                });
                scope.fileAdded({ 'data': response.Data[0] });
                file.upload.then(function (response) {
                  
                    $timeout(function () {
                    file.result = response.data;
                    currItem.thumbUrl = 'http://10.3.12.119/api/Resource/getthumb/' + currItem.Guid;
                    scope.totalFiles.push(currItem);
                  });

                }, 
                function (response) {
                  //alert(response);
                  if (response.status > 0)
                      scope.errorMsg = response.status + ': ' + response.data;
                },
                function (evt) {
                  file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
          });
        });
      }

      scope.DeleteResource = function ($event, item) {
          var items = _.remove(scope.totalFiles, function (n) { return n.Guid != item.Guid; });
          scope.totalFiles = [];
          if (items.length > 0) {
              scope.totalFiles = items;
          }
          scope.fileAdded({ 'data': items, 'isNewList': true });
      }
    
      $timeout(function () {
          scope.openResourceViewer = function (resource) {
              scope.openViewer({ 'resource': resource, 'totalFiles': scope.totalFiles });
          }
      }, 100);

      function GetResourceIds(filename) {
        //var url = 'api/ResourceUploader/';
        var params = {};
        params.id = filename;
        params.bucketId = 1;
        params.apiKey = undefined;
        params.sourceTypeId = scope.userData.UserId;
        params.sourceTypeName = scope.FullName;
        params.sourceId = null;
        params.Source = scope.FullName;
        
        var url = 'resource/GetResourceIds?id='+params.id+'&bucketId='+params.bucketId+'&apiKey='+params.apiKey+'&sourceTypeId='+params.sourceTypeId+'&sourceTypeName='+params.sourceTypeName+'&sourceId='+params.sourceId+'&Source='+params.Source+'';
        //var url = 'category/GetCategoryByTerm/'+ text;
        //scope.gg = Restangular.one(url);
        //scope.gg.get();
        return Restangular.one(url).get();
      }

      function getNewsResources(newsId){
        var news  =  getNewsDetails(newsId).then(function(response){
            var resourceUrl = 'http://10.3.12.119/api/Resource/getthumb/';
            if (response.Data && response.Data.Resources) {
                for (var i = 0; i < response.Data.Resources.length; i++) {
                    var file = {};
                    file.progress = 100;
                    file.$ngfBlobUrl = response.Data.Resources[i].Guid ? resourceUrl + response.Data.Resources[i].Guid : 'http://10.3.12.119/content/images/producer/noimage-730x380.jpg';
                    scope.totalFiles.push(file);
                }
            }
          //console.log(scope.resources);
        });
      }



      function getNewsDetails(){
        var url = 'newsfile/getnews/'+newsId;
        return Restangular.one(url).get();
      }


      function debounce(fn, delay) {
          var timer = null;
          return function () {
              var context = this, args = arguments;
              clearTimeout(timer);
              timer = setTimeout(function () {
                  fn.apply(context, args);
              }, delay);
          };
      }

      addEventListener("message", debounce(function (e) { receivedResource(e); }, 200), false);      
      function receivedResource(event) {
          var expResource = event.data;
          var value = JSON.parse(expResource);
          if (expResource && value.Guid) {
              var obj = {
                  CreationDateStr: value.CreationDateStr,
                  Guid: value.Guid,
                  LastUpdateDateStr: value.CreationDateStr,
                  ResourceTypeId: value.ResourceType
              }
              var file = {};
              file.progress = 100;
              var resourceUrl = 'http://10.3.12.119/api/Resource/getthumb/';
              //file.$ngfBlobUrl = obj.Guid ? resourceUrl + obj.Guid : 'http://10.3.12.119/content/images/producer/noimage-730x380.jpg';
              //scope.totalFiles.push(file);
              file.thumbUrl = obj.Guid ? resourceUrl + obj.Guid : 'http://10.3.12.119/content/images/producer/noimage-730x380.jpg';
              file.Guid = obj.Guid;
              file.ResourceTypeId = obj.ResourceTypeId;
              scope.totalFiles.push(file);
              scope.fileAdded({ 'data': obj, 'isNewList': false });
          }
      }

  }
}
