'use strict';

module.exports = controller;

/* @ngInject */
function controller($log, Restangular, dataservice, $scope, $rootScope, toastr) {
    var vm = this;


    vm.startDate = "";
    vm.endDate = "";
    vm.stDate = "";
    vm.enDate = "";
    //$scope.myVar = false;
    vm.Loader = false;
    vm.AllSelfies = [];
    vm.OgList = [];
    vm.currentIndex = 0;

    vm.onDateRangeChange = function (startDate, endDate) {
        vm.startDate = startDate;
        vm.endDate = endDate;
        vm.WhatsAppData = [];
        vm.Apply(startDate, endDate);
    }

    //Apply Button
    vm.Apply = function (startDate, endDate) {
        vm.Loader = true;
        var dd = startDate.getDate();
        var mm = startDate.getMonth() + 1;
        var yyyy = startDate.getFullYear();
        var stdate = yyyy + '/' + mm + '/' + dd;
        vm.stDate = stdate.toString();

        var edd = endDate.getDate();
        var emm = endDate.getMonth() + 1;
        var eyyyy = endDate.getFullYear();
        var endate = eyyyy + '/' + emm + '/' + edd;
        vm.enDate = endate.toString();

        var postData = {
            from: vm.stDate,
            to: vm.enDate,
        };
        dataservice.GetWhatsAppMsgs(postData).then(function (data) {

            if (data && data.length > 0) {
                var temp = [];

                var WhatsAppData = JSON.parse(data);
                for (i = 0; i < WhatsAppData.length; i++) {
                    if (WhatsAppData[i].ReplyStatus != 1) {
                        WhatsAppData[i].detail = {};
                        WhatsAppData[i].detail.Gender = 'male';

                        if (i < 50) {
                            temp.push(WhatsAppData[i]);
                        }
                    }
                }

                vm.currentIndex = 50;
                vm.WhatsAppData = temp;
                vm.OgList = WhatsAppData;
                vm.Loader = false;
            }
        });
    };

    vm.onScrollWindow = function () {
        var k = 0;
        var ptemp = [];
        for (var i = vm.currentIndex; i <= vm.OgList.length - 1; i++) {
            if (k < 50) {
                ptemp.push(vm.OgList[i]);
            }
            else
                break;

            k = k + 1;
        }
        $rootScope.safeApply(function () {
            vm.WhatsAppData = vm.WhatsAppData.concat(ptemp);
        });
        vm.currentIndex = vm.currentIndex + k;
    }

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            vm.onScrollWindow();
        }
    });

    vm.IsPhoneValid = function (phone) {
        var regex = '/([+0-9])\w+/g';
        var st = phone.toString();
        if (st) {
            var res = st.match(regex);
        }
    }

    //Edit Form
    vm.PostEditForm = function (WhatsAppData, detail, contact) {
        if (detail.Name && detail.Name.trim() == '') {
            return toastr.warning('Name is mandatory !');
        }

        if (detail.City && detail.City.trim() == '') {
            return toastr.warning('City is mandatory !');
        }

        if (detail.Country && detail.Country.trim() == '') {
            return toastr.warning('Country is mandatory !');
        }

        if (detail.Phone && detail.Phone.toString().length > 15) {
            return toastr.warning('Max length of Phone number is 15 !');
        }
        if (detail.Age && detail.Age.toString().length > 3) {
            return toastr.warning('Enter Correct Age !');
        }
        if (detail.Area && detail.Area.length.trim() > 100) {
            return toastr.warning('Max length of Area is 100 !');
        }
        if (detail.City && detail.City.length.trim() > 50) {
            return toastr.warning('Max length of City is 50 !');
        }
        if (detail.Occupation && detail.Occupation.length.trim() > 30) {
            return toastr.warning('Max length of city is 30 !');
        }
        if (detail.Country && detail.Country.length.trim() > 70) {
            return toastr.warning('Max length of country is 70 !');
        }

        var postData = {
            ContestantIdentity: WhatsAppData.ContestantId,
            Name: detail.Name,
            ContactNo: contact,
            Age: detail.Age,
            Address: detail.Area,
            Gender: detail.Gender,
            City: detail.City,
            Occupation: detail.Occupation,
            Country: detail.Country
            //Email: detail.Email,
            //Address: detail.Address,
            //FbProfile: detail.fbProfile,
            //TwitterProfile: detail.twitterProfile,
        };


        dataservice.PostEditForm(postData).then(function (WhatsAppMsgs) {
            if (WhatsAppMsgs == true) {
                _.filter(vm.WhatsAppData, function (n, i) {
                    if (n.ContestantId && WhatsAppData.ContestantId) {
                        if (n.ContestantId == WhatsAppData.ContestantId) {
                            vm.WhatsAppData.splice(i, 1);
                            return toastr.success("Record Updated Successfully !");
                        }
                    }
                });
            }
            else {
                return toastr.warning('Record Update Failed !');
            }
        });

    };
}