﻿//'use strict';

module.exports = controller;

/* @ngInject */

function controller($log, Restangular, dataservice, toastr, $state, $location, $timeout, rabbitmq, $scope, Config, $rootScope) {

    var vm = this;
    var defaultFilterId = Config.DefaultFilterId;
    vm.IsManager = userservice.isManager();
    vm.IsEditorial = userservice.isEditorial();
    userservice.loadInitialData();

    function init() {

    }

    vm.onDateRangeChange = function (startDate, endDate) {
        vm.Date = '';
        var dd = startDate.getDate();
        var mm = startDate.getMonth() + 1;
        var yyyy = startDate.getFullYear();
        var stdate = yyyy + '/' + mm + '/' + dd;
        vm.stDate = stdate.toString();

        var edd = endDate.getDate();
        var emm = endDate.getMonth() + 1;
        var eyyyy = endDate.getFullYear();
        var endate = eyyyy + '/' + emm + '/' + edd;
        vm.enDate = endate.toString();

    
    }
   

}


