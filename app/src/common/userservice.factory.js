'use strict';

module.exports = factory;

/* @ngInject */
function factory($state, localStorageService, Config, dataservice) {
    return {
        isLoggedIn: isLoggedIn,
        logout: logout,
        getUserId: getUserId,
        isEditorial: isEditorial,
        isManager: isManager,
        loadInitialData: loadInitialData,
        getUserName: getUserName,
        getUserRole: getUserRole,
        getPrograms: getPrograms,
        getUserInfo: getUserInfo,
        getUserLocationId: getUserLocationId
    };

    function isLoggedIn() {
        var userInfo = localStorageService.get('userInfo');
        if (userInfo && userInfo.UserId) {
            return true;
        }
        return false;
    }

    function getUserInfo() {
        var userInfo = localStorageService.get('userInfo');
        return userInfo;
    }

    function logout() {
        localStorageService.set('userInfo', null);
        //$state.go('login');
        window.location.href = '/login.cshtml#/index';
    }

    function getUserId() {
        var userInfo = localStorageService.get('userInfo');
        return userInfo.UserId;
    }
    function getUserLocationId() {
        var userInfo = getCookie('LocationId');
        return userInfo;
    }
    function getUserRole() {
        var userInfo = localStorageService.get('userInfo');
        return userInfo.Modules.length > 0 ? userInfo.Modules[0].WorkRoleId : 0;
    }

    function isManager() {
        var userInfo = localStorageService.get('userInfo');
        return userInfo.Modules[0].WorkRoleId == Config.MangerRoleId;
    }
    function isEditorial() {
        var userInfo = localStorageService.get('userInfo');
        return userInfo.Modules[0].WorkRoleId == Config.EditorialRoleId;
    }

    function loadInitialData() {
        var userInfo = localStorageService.get('userInfo');
        dataservice.LoadInitialData(userInfo.UserId, userInfo.Modules[0].WorkRoleId);
    }

    function getUserName(userId) {
        var allUsers = localStorageService.get('allUsers');
        if (allUsers) {
            var user = _.filter(allUsers.users, function (u) { return u.UserId == userId; })[0];
            if (user) {
                return user.Name;
            } else {
                return 'Anonymous';
            }
        }
    }

    function getPrograms() {
        var userData = localStorageService.get('userInfo');
        var programsMeta = _.filter(userData.MetaData, function (meta) { return meta.MetaTypeId == 1018 });

        var programsData = [];
        $.each(programsMeta, function (i, v) {
            programsData.push({ id: v.MetaValue, name: v.MetaName });
        });
        return programsData;
    }

    function getCookie (cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
        }
        return "";
    }
}
