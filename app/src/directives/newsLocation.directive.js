'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice) {
    return {
        link: link,
        restrict: 'E',
        template: require('./newsLocation.directive.html'),
        scope: {
            test: '=',
            locationId: '=',
            locationName: '=',
            onSelectLocation: '&',
            clearText: '='
        }
    };


    function link(scope, elem, attrs) {

        scope.$watch('model', function (newVal, oldVal) {
            if (newVal != oldVal) {
                console.log(newVal)
            }
        });

        setTimeout(function () {
            if (scope.locationId) {
                data.push({ id: scope.locationId, text: scope.locationName });
            }
            var cat = $(elem).find("select");
            cat.select2({
                ajax: {
                    url: function (params) {
                        //return "/api/location/getlocationbyterm/" + params.term;
                        return "/api/socialmediahashtag/GetLocationByTerm?term=" + params.term;
                    },
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        $.each(data, function (i, v) { v.id = v.id; v.text = v.location; v.name = v.location; });

                        return {
                            results: data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 1,
                templateResult: function (entity) {
                    if (entity.loading) return entity.text;
                    return entity.name;
                },
                templateSelection: function (entity) {
                    return entity.name || entity.text;
                },
            });

            scope.$watch('clearText', function () {
                if (scope.clearText == true) {
                    cat.select2("val", 0);
                }
            });

            cat.on('select2:select', function (evt) {
                scope.onSelectLocation({ 'locationId': cat.val()[0] });
            });
        }, 300);        
    }

}
