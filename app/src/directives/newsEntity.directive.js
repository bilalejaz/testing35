'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice) {
    return {
        link: link,
        restrict: 'E',
        template: require('./newsEntity.directive.html'),
        scope: {
            test: '=',
            onSelectEntity: '&',
            clearText: '='
        }
    };


    function link(scope, elem, attrs) {

        scope.$watch('model', function (newVal, oldVal) {
            if (newVal != oldVal) {
                console.log(newVal)
            }
        });

        setTimeout(function () {
            var cat = $(elem).find("select");
            cat.select2({
                tags: true,
                placeholder: "Enter Entity...",
                maximumSelectionLength: 20,
                ajax: {
                    url: function (params) {
                        return "/api/tag/gettagbyterm/" + params.term;
                    },
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data, params) {
                        params.page = params.page || 1;
                        $.each(data.Data, function (i, v) { v.id = v.TagId; v.text = v.Tag; v.name = v.Tag; });

                        

                        return {
                            results: data.Data
                        };
                    },
                    cache: true
                },
                minimumInputLength: 3,
                templateResult: function (entity) {
                    if (entity.loading) return entity.text;
                    return entity.name;
                },
                templateSelection: function (entity) {
                    scope.onSelectEntity({ 'entity': entity.name || entity.text });
                    return entity.name || entity.text;
                },
            });
            scope.$watch('clearText', function () {
                if (scope.clearText == true) {
                    cat.select2('val', 0);
                }
            });
        }, 300);
    }

}
