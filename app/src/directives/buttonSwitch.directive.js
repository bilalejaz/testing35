'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, userservice) {

    return {
        link: link,
        restrict: 'E',
        template: require('./buttonSwitch.directive.html'),
        scope: {
            onSelectWinner: '&',
            deselectAll: '=',
            markWinner: '='
        }
    };

    function link(scope, elem, attrs) {
        scope.status = true;

        scope.changeStatus = function () {
            scope.status = !scope.status;
        }

        scope.checker = function () {
            scope.onSelectWinner({ 'chkvalue': scope.Selection });
        }

        scope.$watch('markWinner', function () {
            if (scope.markWinner) {
                scope.Selection = true;
            }
            else {
                scope.Selection = false;
            }
        });
    }
}
