﻿'use strict';

module.exports = controller;
/* @ngInject */
function controller($log, Restangular, dataservice, toastr, $state, $location, $timeout, rabbitmq, $scope, Config, $rootScope) {

    var vm = this;
    vm.Date = '';
    vm.Loader = false;
    vm.stDate = '';
    vm.enDate = '';
    vm.ShowData = false;
    vm.CurrentGameType = 0;
    vm.IsQuizGame = false;
    vm.QuizGameFirstBallot = false;
    vm.QuizGameSecondBallot = false;
    vm.IsImageVideo = false;
    vm.IsImageVideoBalloting = false;
    vm.IsImageVideoSecondBallot = false;
    vm.IsSecondBalloting = false;
    vm.IsFinalUsers = false;
    vm.ImageVideoResults = [];
    vm.QuizGameFirstBallot = false;
    vm.QuizFirstBallotingResults = [];
    vm.IsImageVideoBalloting = false;
    vm.BallotedUsersList = [];
    vm.IsFinalUsers = false;
    vm.QuizResults = [];
    vm.QuizFirstBallotingResults = [];
    vm.QuesAnsFirstBallotingResults = [];
    vm.FinalBallotingResults = [];
    vm.BallotedUsersList = [];
    vm.startTime = '';
    vm.endTime = '';
    vm.WinnerImage = '';
    vm.WinnerData = '';


    vm.GameTypeOptions = [{ name: 'Select Game', value: 0 }];

    function Init() {
        $('input[type="time"][value="starttime"]').each(function () {
            var d = new Date(),
                h = '00',
                m = '00';
            $(this).attr({
                'value': h + ':' + m
            });
        });

        $('input[type="time"][value="endtime"]').each(function () {
            var d = new Date(),
                h = d.getHours(),
                m = d.getMinutes();
            if (h < 10) h = '0' + h;
            if (m < 10) m = '0' + m;
            vm.EndTime = h + ':' + m;
            $(this).attr({
                'value': h + ':' + m
            });
        });

        dataservice.GetAllGameType().then(function (data) {
            if (data && data.length > 0) {
                for (var m = 0; m <= data.length - 1; m++) {
                    if (data[m].id != 1 && data[m].id != 2)
                    vm.GameTypeOptions.push({ name: data[m].name, value: data[m].id });
                }
            }
        });
    }

    vm.onDateSelect = function (NewDate) {
        vm.Date = '';
        var dd = NewDate.getDate();
        var mm = NewDate.getMonth() + 1;
        var yyyy = NewDate.getFullYear();
        var stdate = yyyy + '/' + mm + '/' + dd;
        vm.Date = stdate.toString();
        console.log(vm.Date);
    }

    vm.onTimeChange = function (NewTime) {
        console.log(NewTime);
    }

    vm.GameTypeSelection = function (val) {
        vm.CurrentGameType = val;
    }

    vm.StartBalloting = function () {
        vm.WinnerImage = '';
        vm.IsQuizGame = false;
        vm.QuizGameFirstBallot = false;
        vm.IsImageVideoBalloting = false;
        vm.IsSecondBalloting = false;
        vm.IsImageVideoSecondBallot = false;
        vm.IsFinalUsers = false;
        vm.QuizFirstBallotingResults = [];
        vm.QuizResults = [];
        vm.QuizFirstBallotingResults = [];
        vm.QuesAnsFirstBallotingResults = [];
        vm.FinalBallotingResults = [];
        vm.BallotedUsersList = [];
        vm.BallotedUsersList = [];
        vm.IsFinalUsers = false;
        vm.WinnerData = '';
        var startTime = $('.st_time').val();
        var endTime = $('.en_time').val();
        vm.startTime = startTime;
        vm.endTime = endTime;

        if (startTime == '') {
            return toastr.warning('Please Enter Start Time');
        }
        if (endTime == '') {
            return toastr.warning('Please Enter End Time');
        }

        if (startTime != '' && endTime != '') {
            var s = startTime.split(":");
            var e = endTime.split(":");
            var a = parseInt(s[0]);
            var b = parseInt(s[1]);
            var c = parseInt(e[0]);
            var d = parseInt(e[1]);

            if (a > c) {
                return toastr.warning('Please Enter End Time');
            }
            if (a == c && b > d) {
                return toastr.warning('Please Enter End Time');
            }
        }

        if (vm.CurrentGameType > 0) {
            if (vm.CurrentGameType == 3) {
                dataservice.GetQuizResults(vm.Date, startTime, endTime, vm.CurrentGameType).then(function (data) {
                    if (data.quizAnswerReport && data.quizAnswerReport.length > 0) {
                        vm.QuizResults = data.quizAnswerReport;
                        vm.IsQuizGame = true;

                        setTimeout(function () {
                            $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight;
                        }, 1000);

                    }
                });
            }
            else if (vm.CurrentGameType != 3) {
                dataservice.GetQuizResults(vm.Date, startTime, endTime, vm.CurrentGameType).then(function (data) {
                    if (data.mediaReport && data.mediaReport.length > 0) {
                        vm.ImageVideoResults = data.mediaReport;
                        vm.IsImageVideoBalloting = true;

                        setTimeout(function () {
                            $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight;
                        }, 1000);

                    }
                });
            }
        }
        else {
            return toastr.warning('Please Select Game Type');
        }

    }

    vm.ViewGameBalloting = function (questionNumber) {
        debugger;
        vm.FirstBallotingResults = [];
        vm.Question = questionNumber;
        if (vm.CurrentGameType == 3) {
            dataservice.GetAnswerDetailsForBalloting(questionNumber, vm.Date, vm.startTime, vm.endTime, vm.CurrentGameType).then(function (data) {
                if (data) {
                    vm.QuizGameFirstBallot = true;
                    vm.QuizFirstBallotingResults = data;
                    setTimeout(function () { $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight; }, 1000);
                }
                else {
                    return toastr.warning('No Record!');
                }
            });
        }
        else {
            dataservice.GetAnswerDetailsForBalloting(questionNumber, vm.Date, vm.startTime, vm.endTime, vm.CurrentGameType).then(function (data) {
                if (data.cityViseCout && data.cityViseCout.length > 0) {
                    vm.IsImageVideoBalloting = true;
                    vm.QuesAnsFirstBallotingResults = data;
                    setTimeout(function () { $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight; }, 1000);
                }
                else {
                    return toastr.warning('No Record!');
                }
            });
        }
    }

    vm.SubmitFirstBallot = function () {
        debugger;
        vm.FinalQABallotingResults = [];
        vm.FinalIVBallotingResults = [];
        if (vm.FirstBallotCount) {
            if (vm.CurrentGameType == 3) {
                dataservice.GetQuesAnsFirstBallotedResults(vm.Date, vm.startTime, vm.endTime, vm.CurrentGameType, vm.FirstBallotCount, vm.Question).then(function (data) {
                    if (data) {
                        vm.QuizGameSecondBallot = true;
                        vm.FinalQABallotingResults = data;
                        setTimeout(function () { $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight; }, 1000);
                    }
                    else {
                        return toastr.info('No Record. Ballot Again !');
                    }
                });
            }
            else if (vm.CurrentGameType != 3) {
                dataservice.GetCorrectAnserDetail(vm.Question, vm.Date, vm.startTime, vm.endTime, vm.CurrentGameType, vm.FirstBallotCount).then(function (data) {
                    if (data.cityViseCout && data.cityViseCout.length > 0) {
                        vm.IsSecondBalloting = true;
                        vm.FinalIVBallotingResults = data;
                        setTimeout(function () { $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight; }, 1000);
                    }
                    else {
                        return toastr.info('No Record. Ballot Again !');
                    }
                });
            }
        }
        else {
            return toastr.warning('Please Enter Balloted Count!');
        }
    }

    vm.GetBallotedUsers = function () {
        vm.BallotedUsersList = [];
        if (vm.FinalBallotCount) {
            dataservice.GetBallotedUsersResults(vm.Date, vm.startTime, vm.endTime, vm.CurrentGameType, vm.FinalBallotCount, vm.Question).then(function (data) {
                if (data && data.length > 0) {
                    for (var i = 0; i <= data.length - 1; i++) {

                        if (data[i].mediaSourceType == '1') {
                            data[i].mediaSourceName = 'Twitter';
                        }
                        if (data[i].mediaSourceType == '2') {
                            data[i].mediaSourceName = 'Facebook';
                        }
                        if (data[i].mediaSourceType == '3') {
                            data[i].mediaSourceName = 'WhatsApp';
                        }
                        if (data[i].mediaSourceType == '4') {
                            data[i].mediaSourceName = 'Email';
                        }

                        data[i].IsWinner = false;
                        data[i].IsTrueWinner = false;

                    }

                    vm.BallotedUsersList = data;
                    vm.IsFinalUsers = true;

                    setTimeout(function () { $('.btn_head')[0].scrollTop = $('.btn_head')[0].scrollHeight; }, 1000);
                }
            });
        }
        else {
            return toastr.warning('Please Enter Balloted Count!');
        }
    }

    vm.onSelectWinner = function (data, chkValue, AllData) {
        for (var n = 0; n <= AllData.length - 1; n++) {
            AllData[n].IsWinner = false;
            AllData[n].IsTrueWinner = false;
        }

        if (chkValue) {
            data.IsWinner = chkValue;
            data.IsTrueWinner = true;
        }
    }

    vm.ApproveWinner = function (data) {
        
        if (data.id && data.id > 0) {
            dataservice.MarkWinner(vm.CurrentGameType, 0, vm.Date, data.id).then(function (res) {
                if (res) {
                    vm.IsQuizGame = false;
                    vm.IsFirstBalloting = false;
                    vm.IsSecondBalloting = false;
                    vm.IsFinalUsers = false;
                    vm.WinnerImage = data.primarySelfie;
                    vm.WinnerData = data;
                    $(".pass_popup1").css('display', 'initial');
                    return toastr.success('Winner Marked Successfully !');
                }
                else {
                    return toastr.warning('Contestant Not Available !');
                }
            });
        }
        else {
            return toastr.warning('Contestant Not Available !');
        }
    }

    vm.closeIframe = function () {
        $(".pass_popup1").css('display', 'none');
        vm.showImageViewer = false;
        vm.selfiesOfUser = [];
    }


    Init();
}