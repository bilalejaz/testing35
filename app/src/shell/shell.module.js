'use strict';

//require('./shell.scss');

var name = module.exports = 'BolSelfie.shell';

angular
  .module(name, [])
  .config(configuration)
  .controller('Shell', require('./shell.controller.js'))
;

function configuration($stateProvider) {
  $stateProvider
    .state('shell', {
        url: '/',
        views: {
            '@': {
                template: require('./shell.html'),
                controller: 'Shell as vm'
            },
        },
        bodyClass: ''
    });
}
