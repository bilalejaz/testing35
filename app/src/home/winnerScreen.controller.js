'use strict';

module.exports = controller;

/* @ngInject */
function controller($log, Restangular, dataservice, $scope, $rootScope, toastr) {
    var vm = this;

    vm.Loader = false;
    vm.startDate = "";
    vm.endDate = "";
    vm.LocationId = 0;
    vm.currentPrizeType = 1;
    vm.gameType = 1;
    vm.Price = [{ name: 'select PriceType', value: 0 }];
    vm.Game = [{ name: 'select GameType', value: 0 }];

    vm.onDateRangeChange = function (startDate, endDate) {
        vm.startDate = startDate;
        vm.endDate = endDate;
        vm.records = [];
    }

    vm.onSelectLocation = function (LocationId) {
        vm.LocationId = LocationId;
    }


    vm.getPrizeType = function () {
        dataservice.getPrizeType().then(function (PriceType) {
            for (var i = 0; i <= PriceType.length - 1; i++) {
                vm.Price.push({ name: PriceType[i].name, value: PriceType[i].id });
            }
        });
    };


    vm.getGameType = function () {
        dataservice.getGameType().then(function (GameType) {
            // vm.Game = GameType;
            for (var i = 0; i <= GameType.length - 1; i++) {
                vm.Game.push({ name: GameType[i].name, value: GameType[i].id });
            }
        });
    }


    vm.Apply = function (startDate, endDate, PrizeType, GameType, LocationId) {
        vm.records = [];

        if (PrizeType == 0) {
            toastr.warning("Please Select Prize Type!");
        }

        else if (GameType == 0) {

            toastr.warning("Please Select Game Type!");

        }
        else {
            vm.Loader = true;
            var postData = {
                StartDate: startDate,
                EndDate: endDate,
                GiftType: PrizeType,
                GameType: GameType,
                LocationId: LocationId,

            };

            dataservice.LoadWinnersData(postData).then(function (records) {
                if (records.length > 0) {
                    vm.Loader = false;
                    vm.records = records;

                }
                else {
                    vm.Loader = false;

                }
            });
        }
        // alert(PrizeType+" "+GameType);

    };

    //Search by Name TextBox
    //   vm.GetApiSearch = function () {
    //   dataservice.GetApiSearch().then(function (records) {
    //       vm.records = records;

    //     });
    //};


    //vm.GetApiSearch();
    vm.getPrizeType();
    vm.getGameType();
}
