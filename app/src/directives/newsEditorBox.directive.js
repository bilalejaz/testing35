'use strict';

module.exports = directive;

/* @ngInject */
function directive($log) {

  return {
    link: link,
    restrict: 'AE',
    template: require('./newsEditorBox.directive.html'),
    scope: {
        editingInside: '=',
        onSelectLanguage:'&'
    }
  };

  function link(scope, elem, attrs){
    var defaultLanguage = 1;
    //scope.languages = [{ key: 1, label: "UR", text: 'اردو' }, { key: 2, label: "EN", text: 'Eng' }];
    scope.languages = [{ key: 1, label: "ur" }, { key: 2, label: "en" }];


    var input = $(elem).find('.editBox.urduDv .inputRow > input, .editBox.urduDv .inputRow > textarea');
    $(input).UrduEditor();

    $(input).on('focus',function(){
      $(this).parents('news-editor-box').addClass('togl');
    }).on('blur',function(){
      $(this).parents('news-editor-box').removeClass('togl');
    });

    //$(input).css('font-family', "'Jameel Noori Nastaleeq', 'Nafees nastaleeq', 'Nafees Web Naskh', Helvetica, sans-serif");
    //$(input).css('backgroundColor', 'rgb(245, 245, 245)');
    //$(input).css('fontSize', '125%');
    scope.languageName = defaultLanguage == 1 ? 'ur' : 'en';
    scope.butAct = 1;

    scope.toggleDropdown = function () {
      scope.showOptions = !scope.showOptions;
    }

    scope.changeOnTextBox = function (tickerText) {
      //console.log(tickerText);
    };
    $('input').on('change keyup', function () {
      scope.tickerText = $(this).val();
    });
    scope.languageChange = function (languageCode) {
        scope.selectedLanguage = languageCode;
        
        scope.onSelectLanguage({ 'lang': languageCode});
    };
    var languageWorking = function (languageCode) {
      scope.languageName = languageCode == 1 ? 'ur' : 'en';
      scope.showOptions = false;
      scope.butAct = languageCode;
      $(input).UrduEditor.ToggleLanguage();


      if (languageCode == 1 && scope.selected != languageCode) {
        //$(input).UrduEditor.ToggleLanguage();
        //$(input).attr('dir', 'rtl');
        //$(input).parents('.editBox').addClass('urduDv');
      } else if (languageCode == 2 && scope.selected != languageCode) {
        //$(input).UrduEditor.ToggleLanguage();
        //$(input).removeAttr('dir');
        //$(input).parents('.editBox').removeClass('urduDv');
      }

    };
    if (!scope.selectedLanguage) {
      scope.selectedLanguage = defaultLanguage;
    }

    var keys = [];
    $(document).on('keyup','.editBox',function (e) {
        keys.push(e.which);
        //console.log(e);
      if(keys.indexOf(17) >= 0 && keys.indexOf(32) >= 0){
        (scope.butAct == 1) ? (scope.butAct = 2) : (scope.butAct = 1);
      }
      setTimeout(function () { keys = []; }, 100);
      var element = angular.element($(e.target));
      element.triggerHandler('input'); // for urdu typing it updates model for each keypress
    });

   

    //scope.$watch('selectedLanguage', function (newVal, oldVal) {
    //  if (newVal != oldVal) {
    //    languageWorking(scope.selectedLanguage);
    //  }
    //});

    //console.log(scope.editingInside);
    if (__DEV__) { // eslint-disable-line no-undef
      $log.info('Initializing newsEditorBox directive');
    }
  }
}
