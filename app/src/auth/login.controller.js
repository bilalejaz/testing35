'use strict';

module.exports = controller;

/* @ngInject */
function controller($state, dataservice, localStorageService, userservice) {
    var vm = this;

    function init() {
        //if (userservice.isLoggedIn()) {
            $state.go('shell.bolselfieverification');
        //}
    }

    vm.login = function () {

        localStorageService.set('userName', vm.username);
        dataservice.login(vm.username, vm.password).then(function (response) {
            if (response.IsSuccess) {
                localStorageService.set('userInfo', response.Data);
                

                $state.go('shell.bolselfieverification');
            }
        });
    }

    init();
}
