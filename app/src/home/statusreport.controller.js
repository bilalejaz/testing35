﻿'use strict';

module.exports = controller;
/* @ngInject */
function controller($log, Restangular, dataservice, toastr, $state, $location, $timeout, rabbitmq, $scope, Config, $rootScope) {

    var vm = this;
    vm.reportList = [];
    vm.onDateRangeChange = function (startDate, endDate) {
        vm.Loader = true;
      
        var dd = startDate.getDate();
        var mm = startDate.getMonth() + 1;
        var yyyy = startDate.getFullYear();
        var stdate = yyyy + '/' + mm + '/' + dd;
        vm.stDate = stdate.toString();

        var edd = endDate.getDate();
        var emm = endDate.getMonth() + 1;
        var eyyyy = endDate.getFullYear();
        var endate = eyyyy + '/' + emm + '/' + edd;
        vm.enDate = endate.toString();

        var reqObj = {
            from: vm.stDate,
            to: vm.enDate
        }
        dataservice.SelfieReport(reqObj).then(function (data) {
            if (data && data.length > 0) {
                var result = JSON.parse(data);
                vm.reportList = result;
                vm.Loader = false;
            }
            else {
                vm.Loader = false;
            }
        });
    }

    function Init() {
     
    }

    Init();
}