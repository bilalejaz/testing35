'use strict';

//require('./home.scss');

var name = module.exports = 'BolSelfie.home';

angular
  .module(name, [])
  .config(configuration)
    .controller('BolSelfieVerification', require('./bolselfieverification.controller.js'))
    .controller('winnerScreen', require('./winnerScreen.controller.js'))
	.controller('WhatsappTextMsgs', require('./WhatsappTextMsgs.controller.js'))
    .controller('Home', require('./home.controller.js'))
    .controller('Balloting', require('./balloting.controller.js'))
    .controller('StatusReport', require('./statusreport.controller.js'))
    .controller('SelfieGameBalloting', require('./selfiegameballoting.controller.js'))
    .controller('Contestants', require('./contestants.controller.js'))
    .controller('weightage', require('./weightage.controller.js'));

function configuration($stateProvider) {

    $stateProvider
    .state('shell.bolselfieverification', {
        url: 'bolselfieverification',
        title: 'Bol Selfie Verification',
        views: {
            'content@shell': {
                template: require('./bolselfieverification.html'),
                controller: 'BolSelfieVerification as vm'
            }
        }
    }).state('shell.home', {
        url: 'home',
        title: 'Bol Users Selfie',
        views: {
            'content@shell': {
                template: require('./home.html'),
                controller: 'Home as vm'
            }
        }
    }).state('shell.balloting', {
        url: 'Balloting',
        title: 'Balloting',
        views: {
            'content@shell': {
                template: require('./balloting.html'),
                controller: 'Balloting as vm'
            }
        }
    }).state('shell.winnerScreen', {
        url: 'winnerScreen',
        title: 'WinnerScreen',
        views: {
            'content@shell': {
                template: require('./winnerScreen.html'),
                controller: 'winnerScreen as vm'
            }
        }
    }).state('shell.WhatsappTextMsgs', {
        url: 'WhatsappTextMsgs',
        title: 'WhatsappTextMsgs',
        views: {
            'content@shell': {
                template: require('./WhatsappTextMsgs.html'),
                controller: 'WhatsappTextMsgs as vm'
            }
        }
    }).state('shell.statusreport', {
        url: 'statusReport',
        title: 'StatusReport',
        views: {
            'content@shell': {
                template: require('./statusreport.html'),
                controller: 'StatusReport as vm'
            }
        }
    }).state('shell.selfiegameballoting', {
        url: 'SelfieGameBalloting',
        title: 'SelfieGameBalloting',
        views: {
            'content@shell': {
                template: require('./selfiegameballoting.html'),
                controller: 'SelfieGameBalloting as vm'
            }
        }
    }).state('shell.contestants', {
        url: 'contestants',
        title: 'Contestants',
        views: {
            'content@shell': {
                template: require('./contestants.html'),
                controller: 'Contestants as vm'
            }
        }
    })
    .state('shell.weightage', {
        url: 'weightage',
        title: 'Weightage',
        views: {
            'content@shell': {
                template: require('./weightage.html'),
                controller: 'weightage as vm'
            }
        }
    });
}

