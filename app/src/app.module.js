'use strict';

require('angular');
angular.module('BolSelfie', [
  require('./core/core.module.js'),
  require('./common/common.module.js'),
  require('./home/home.module.js'),
  require('./shell/shell.module.js'),
  require('./auth/auth.module.js'),
  require('./controls/controls.module.js'),
  require('./directives/directives.module.js')
]).factory('$exceptionHandler', function ($log, $injector) {
    return function myExceptionHandler(exception, cause) {
        $log.error(exception, cause);
        var url = window.location.href.toString();
        var localst = localStorage["ls.userName"];
        var CurrUser = '';
        if (localst) {
            localst = localst.toString();
            CurrUser = localst.toString().replace(/"/g, "")
            CurrUser.toString();
        }
        //jsErrorLogging(exception.message, exception.stack, url, exception.lineNumber, CurrUser);
    };
});

function jsErrorLogging(message, stack, url, lineNumber, User) {
    if (message)
        message.toString();
    if (stack)
        stack.toString();
    if (url)
        url.toString();
    if (lineNumber)
        lineNumber.toString();
    if (User)
        User.toString();

    var reqObj = { Message: message, Stack: stack, ExceptionUrl: url, Linenumber: lineNumber, User: User }
    $.post('/api/news/JSErrorLogging', reqObj,
    function (data, status) {
        console.log(status);
    });
}