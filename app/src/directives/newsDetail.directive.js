'use strict';

module.exports = directive;

/* @ngInject */
function directive($log, dataservice, Restangular, userservice) {
    return {
        link: link,
        restrict: 'E',
        template: require('./newsDetail.directive.html'),
        scope: {
            newsId: '='
        }
    };


    function link(scope, elem, attrs, $rootScope) {

      console.log(scope, elem, attrs, attrs.popup, $rootScope);
      (attrs.popup) ? (scope.isPopup = true) : (scope.isPopup = false);

        function getNewsDetail() {
            var url = 'newsfile/getnews?id=' + scope.newsId;
            return Restangular.one(url).get();
        }

        function updateNewsDetail(){
            getNewsDetail().then(function (data) {
                if (data.IsSuccess) {
                    scope.newsDetail = data.Data;
                    scope.newsDetail.Created = moment(scope.newsDetail.CreationDateStr).fromNow();
                    scope.newsDetail.ImageUrl = scope.newsDetail.ResourceGuid ? 'http://10.3.12.119/api/Resource/getthumb/' + scope.newsDetail.ResourceGuid : 'http://10.3.12.118/Content/images/producer/noimage-730x380.jpg';
                    //  scope.newsDetail.ImageUrl2;
                    if (scope.newsDetail.Resources && scope.newsDetail.Resources.length > 0) {
                        for (var i = 0; i < scope.newsDetail.Resources.length; i++) {
                            scope.newsDetail.Resources[i].ImageUrl = scope.newsDetail.Resources[i].Guid ? 'http://10.3.12.119/api/Resource/getthumb/' + scope.newsDetail.Resources[i].Guid : 'http://10.3.12.118/Content/images/producer/noimage-730x380.jpg';
                        }
                    }


                    if (scope.newsDetail.Source && scope.newsDetail.Source == "Google News") {
                        setTimeout(function () {
                            document.getElementById("google_html").innerHTML = scope.newsDetail.Description;
                        }, 500);
                    }

                    if (scope.newsDetail.EventReporter && scope.newsDetail.EventReporter.length > 0) {
                        for (var i = 0; i < scope.newsDetail.EventReporter.length; i++) {
                            scope.newsDetail.EventReporter[i].Name = userservice.getUserName(scope.newsDetail.EventReporter[i].ReporterId || 0);
                        }
                    }
                    if (scope.newsDetail.PublishTimeStr && scope.newsDetail.PublishTimeStr.length > 0) {
                        scope.newsDetail.PublishTimeStr = moment(scope.newsDetail.PublishTimeStr).format('MMMM Do YYYY, h:mm:ss a')
                    }

                    if (scope.newsDetail.TaggedBy && scope.newsDetail.TaggedBy)
                        scope.newsDetail.TaggedUser = userservice.getUserName(responseData.TaggedBy);

                }
            });
        }

        if (scope.newsId && scope.newsId > 0) {
            updateNewsDetail();
        }

        scope.openViewer = function (resource) {
            
            scope.showImageViewer = true;
            scope.selectedResource = resource;
            scope.allResources = scope.newsDetail.Resources;
        }
        scope.closeViewer = function () {
            scope.showImageViewer = false;
        }

        scope.$watch("newsId", function (newVal, oldVal) {
            if ( newVal != oldVal) {
                if (newVal && newVal > 0) {
                    updateNewsDetail();
                }
            }
        });
    }


}
