'use strict';

module.exports = directive;

/* @ngInject */
function directive($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.$watch(function () {
                return scope.$eval(attrs.bindHtmlCompileValue);
            }, function (value) {
                // In case value is a TrustedValueHolderType, sometimes it
                // needs to be explicitly called into a string in order to
                // get the HTML string.
                element.html(value && value.toString());
                // If scope is provided use it, otherwise use parent scope
                var compileScope = scope;
                if (attrs.bindHtmlScope) {
                    compileScope = scope.$eval(attrs.bindHtmlScope);
                }

                var html = element.contents();
                html.prepend('<input type="hidden" ng-init="vm.id=' + attrs.value + '"/>')
                $compile(html)(compileScope);
            });
        }
    };
}
