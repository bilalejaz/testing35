'use strict';

module.exports = directive;

/* @ngInject */
function directive($rootScope, Restangular, dataservice) {
    return {
        link: link,
        restrict: 'E',
        template: require('./imageViewer.directive.html'),
        scope: {
            selectedResource: '=',
            allResources: '=',
            onClose: '&'
        }
    };


    function link(scope, elem, attrs) {
        scope.closeViewer = function () {
            scope.onClose();
          //scope.$root.showImageViewer = false;
        }
        scope.nextResource = function () {
            var currentIndex = _.findIndex(scope.allResources, function (o) { return o.Guid == scope.selectedResource.Guid; });
            scope.selectedResource = scope.allResources[(currentIndex + 1) == scope.allResources.length ? 0 : (currentIndex + 1)];
            getResourceInformation();
        }
        scope.prevResource = function () {
            var currentIndex = _.findIndex(scope.allResources, function (o) { return o.Guid == scope.selectedResource.Guid; });
            scope.selectedResource = scope.allResources[currentIndex == 0 ? (scope.allResources.length - 1) : (currentIndex - 1)];
            getResourceInformation();
        }

        function getResourceInformation() {
            var currentResource = scope.selectedResource;
            currentResource.Url = "http://10.3.12.119/api/Resource/getResource/" + currentResource.Guid;

            setTimeout(function () {
                if (currentResource.ResourceTypeId == 2) {
                    var video = document.getElementById('vPlayer');
                    var source = document.createElement('source');

                    source.setAttribute('src', currentResource.Url);
                    video.appendChild(source);
                    video.play();
                }
            }, 500);


            var url = 'Resource/GetResourceMetaByGuid/?Guid=' + currentResource.Guid;
            Restangular.oneUrl('resources', 'http://10.3.12.119/api/' + url).get().then(function (data) {
                if (data.fromServer) {

                scope.Resources = data;
                }
            });


        }
        getResourceInformation();

    }

}
